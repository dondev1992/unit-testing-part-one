package com.catalyte.training.monkeytesting;
/**
 * @author - Don Moore
 * @version - Java Unit Testing Part 1
 * @task - Write a solution for the Monkey Trouble problem.
 *         Provide unit tests to cover all test cases provided for the Monkey Trouble problem.
 */

import org.junit.Test;

import static org.junit.Assert.*;

public class MonkeyTest {

    MonkeyTrouble monkeyTroubleTest = new MonkeyTrouble();
    @Test
    public void twoTrueSmilesReturnTrue() {

        boolean expectedResult = true;
        boolean actualResult = monkeyTroubleTest.monkeyTrouble(true, true);

        assertEquals("Expected an answer of true",expectedResult, actualResult);
    }

    @Test
    public void twoFalseSmilesReturnTrue() {
        boolean condition = monkeyTroubleTest.monkeyTrouble(false, false);

        assertTrue("Expected an answer of true", condition);
    }

    @Test
    public void twoFalseSmilesReturnFalse() {
        boolean condition = monkeyTroubleTest.monkeyTrouble(false, false);

        assertFalse("Expected an answer of true",condition);
    }

    @Test
    public void firstArgFalseSecondArgTrueShouldReturnFalse() {
        boolean expectedResult = false;
        boolean actualResult = monkeyTroubleTest.monkeyTrouble(false, true);

        assertEquals("Expected to get a result of false",expectedResult, actualResult);
    }

    @Test
    public void firstArgTrueSecondArgFalseShouldReturnFalse() {
        boolean condition = monkeyTroubleTest.monkeyTrouble(true, false);

        assertTrue("Expected to get a result of false", condition);
    }
}