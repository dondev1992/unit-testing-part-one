package com.catalyte.training.monkeytesting;
/**
 * @author - Don Moore
 * @version - Java Unit Testing Part 1
 * @task - Write a solution for the Monkey Trouble problem.
 *         Provide unit tests to cover all test cases provided for the Monkey Trouble problem.
 */
public class MonkeyTrouble {
  /**
   * - This method takes the 2 monkey smile booleans as arguments and returns another boolean and
   * - if true, we are in "trouble". If false, we are not in "trouble"
   * @param aSmile - Represents the smile of the first Monkey type boolean
   * @param bSmile - Represents the smile of the second Monkey type boolean
   * @return - A boolean
   */
  public boolean monkeyTrouble(boolean aSmile, boolean bSmile) {
    boolean trouble = false;

    if (aSmile && bSmile) {
      trouble = true;
    } else trouble = !aSmile && !bSmile;
    return trouble;
  }
}
